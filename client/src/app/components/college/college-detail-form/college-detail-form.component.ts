import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { College } from 'src/app/shared/models/College.model';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Location } from 'src/app/shared/models/Location.model';
import { LocationService } from 'src/app/services/location/location.service';

@Component({
  selector: 'app-college-detail-form',
  templateUrl: './college-detail-form.component.html',
  styleUrls: ['./college-detail-form.component.css'],
})
export class CollegeDetailFormComponent implements OnInit {
  @Input()
  college: College;

  locations: Location[] = [];

  @Input()
  buttonName!: string;

  @Output()
  formSubmit: EventEmitter<any> = new EventEmitter<any>();

  constructor(private locationService: LocationService) {}

  ngOnInit() {
    this.locationService.getAllLocations().subscribe((res) => {
      console.log("fetchedLocations",res);
      this.locations = res;
    });
  }

  collegeFormSubmit(): void {
    this.formSubmit.emit(this.college);
  }
}
