import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authenication/authenication.service';
import { CollegeService } from 'src/app/services/college/college.service';
import { College } from 'src/app/shared/models/College.model';
import { DismissiveAlertComponent } from '../../utils/dismissive-alert/dismissive-alert.component';

@Component({
  selector: 'app-register-college',
  templateUrl: './register-college.component.html',
  styleUrls: ['./register-college.component.css'],
})
export class RegisterCollegeComponent implements OnInit {
  college: College = {
    collegeName: '',
    collegeWebsite: '',
    collegeNirfRanking:0,
    collegeAicteAffiliation: false,
    collegeLocation: {
      locationId: 0
      // locationCity: '',
      // locationCountry: '',
      // locationDistrict: '',
      // locationPinCode: '',
      // locationState: '',
    },
    collegeEmail: '',
  };

  buttonNameRegister: string = 'Register';

  @ViewChild(DismissiveAlertComponent)
  alert!: DismissiveAlertComponent;

  constructor(private collegeService: CollegeService, private authService: AuthenticationService) {}

  ngOnInit(): void {

  }

  onUpdate(college: any) {
    this.collegeService.registerCollege(this.college).subscribe({
      next: (result) => {
        console.log(college);
        console.log(result);
        this.college = result;
        this.alert.add(
          'success',
          'College details registered sucessfully',
          2000
        );
        this.authService.logOut();
      },
      error: (err) => {
        console.log(college);
        console.log(err);
        this.alert.add(
          'danger',
          'Company details could not be registered',
          3000
        );
      },
      complete: () => console.info('complete'),
    });
  }
}
