import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authenication/authenication.service';
import { CollegeService } from 'src/app/services/college/college.service';
import { UserService } from 'src/app/services/user/user.service';
import { College } from 'src/app/shared/models/College.model';

@Component({
  selector: 'app-search-college',
  templateUrl: './search-college.component.html',
  styleUrls: ['./search-college.component.css'],
})
export class SearchCollegeComponent implements OnInit {
  // List of all colleges | 
  colleges: College[] = [];

  constructor(
    private collegeService: CollegeService,
    private userService: UserService, private authService: AuthenticationService) {}

  ngOnInit(): void {
    this.collegeService.getAllColleges().subscribe({
      next: (result) => {
        this.colleges = result;
      },
      error: (err) => {
        console.log(err);
      },
      complete: () => console.info('complete')
    })
  }

  userRequestCollege(college: College) {
    this.collegeService.updateUserCollege(college).subscribe({
      next: (result) => {
        this.authService.logOut();
      },
      error: (err) => {
        console.log(err);
      },
      complete: () => console.info('complete')
    })
  }
}
