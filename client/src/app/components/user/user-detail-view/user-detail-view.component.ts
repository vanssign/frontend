import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UserService } from 'src/app/services/user/user.service';
import { UserType } from 'src/app/shared/enums/UserType-enum.model';
import { User } from 'src/app/shared/models/User.model';
import { UserName } from 'src/app/shared/models/UserName.model';

@Component({
  selector: 'app-user-detail-view',
  templateUrl: './user-detail-view.component.html',
  styleUrls: ['./user-detail-view.component.css'],
})
export class UserDetailViewComponent implements OnInit {
  @Input()
  user!: User;

  @Output()
  logout: EventEmitter<any> = new EventEmitter<any>();

  constructor() {}

  ngOnInit(): void {}

  onLogout(): void {
    this.logout.emit();
  }

  displayUserName(): string {
    if (this.user.userName) {
      if (this.user.userName.middleName) {
        return `${this.user.userName.firstName} ${this.user.userName.middleName} ${this.user.userName.lastName}`;
      } else {
        return `${this.user.userName.firstName} ${this.user.userName.lastName}`;
      }
    } else return '';
  }

  displayUserType():string{
    switch(this.user.userType?.toLocaleString()) { 
      case "USER": { 
         return "User";
         break; 
      } 
      case "COMPANY_ADMIN": { 
         return "Company Admin";
         break; 
      } 
      case "COMPANY_SPOC":{
        return "Company Recruitor";
        break;
      }
      case "COLLEGE_ADMIN": { 
        return "College Admin";
        break; 
     } 
     case "COLLEGE_SPOC":{
       return "College SPOC";
       break;
     }
      default: { 
         return "Type not Defined"; 
         break; 
      } 
   }
  }
}
